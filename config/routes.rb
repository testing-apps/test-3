# frozen_string_literal: true

Rails.application.routes.draw do
  root 'home#index', as: 'home'
  get '/notes' => 'notes#index'
  post '/create_note' => 'notes#create'
  get 'search', to: 'searches#search'
  # Showing user login, logging them in, logging them out
  get '/login' => 'sessions#new'
  post '/login' => 'sessions#create'
  get '/logout' => 'sessions#destroy'

  patch '/password' => 'password#update'
  get '/password' => 'password#edit'
  get '/note_roadblocks/:id' => 'solutions#show'

  resources :notes do
    resources :comments
  end

  resources :note_roadblocks do
    resources :solutions
  end

  resources :teams
  resources :password
  resources :admin do
    member do
      patch :set_admin
      delete :remove_admin
    end
  end
  resources :employees
end
