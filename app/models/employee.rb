# frozen_string_literal: true

class Employee < ApplicationRecord
  has_many :notes
  default_scope { order(:last_name, :first_name) }
  has_and_belongs_to_many :team

  def self.active
    Employee.where.not(users_id: nil).to_a
  end

  def teams
    team_ids = EmployeesTeam.where(employees_id: id, active: true).map(&:teams_id).uniq
    Team.find(team_ids)
  end

  def roadblock_for(note_id)
    NoteRoadblock.where(employees_id: id, note_id: note_id)
  end

  def first_name=(val)
    write_attribute(:first_name, val.strip.capitalize)
  end

  def last_name=(val)
    write_attribute(:last_name, val.strip.capitalize)
  end

  def email_address=(val)
    write_attribute(:email_address, val.strip.downcase)
  end

  def as_user
    User.find_by(id: users_id)
  end

  def to_s
    "#{first_name} #{last_name}"
  end
end
