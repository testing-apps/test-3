# frozen_string_literal: true

class Team < ApplicationRecord
  default_scope { order(:name) }

  SnapIT = Team.find_by(name: 'SnapIT')

  def self.active
    Team.where(active: true)
  end

  def members
    active_employee_ids = EmployeesTeam.where(teams_id: id, active: true).map(&:employees_id)
    Employee.find(active_employee_ids)
  end

  def lead
    Employee.find(lead_id) unless lead_id.nil?
  end

  def self.leads
    Employee.find(Team.active.map(&:lead_id)).to_a.uniq
  end

  def add_member(member_id)
    member_id = member_id.to_i
    return self if members.map(&:id).include?(member_id)

    EmployeesTeam.create(employees_id: member_id, teams_id: id, active: true)
    self
  end

  def remove_member(member_id)
    member_id = member_id.to_i
    return self unless members.map(&:id).include?(member_id)

    EmployeesTeam.find_by(employees_id: member_id, teams_id: id, active: true).update(active: false)
    self
  end

  after_initialize do
    self.active ||= true if new_record?
  end
  has_many :employees_teams, dependent: :delete_all
  has_and_belongs_to_many :employees
end
