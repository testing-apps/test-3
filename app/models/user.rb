# frozen_string_literal: true

class User < ApplicationRecord
  has_secure_password validations: false

  # Return all user id's of users set as admins
  def self.admin_users
    UserRole.where(role: 'admin').pluck(:users_id)
  end

  def roles
    UserRole.where(users_id: id).map(&:role)
  end

  def admin?
    roles.any?('admin')
  end

  # Return employee id that belongs to the users_id passed
  def as_employee
    Employee.find_by_users_id(id)
  end

  # Return
  def user_name=(val)
    write_attribute(:user_name, val.strip.downcase)
  end
end
