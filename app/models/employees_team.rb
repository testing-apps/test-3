# frozen_string_literal: true

class EmployeesTeam < ApplicationRecord
  def self.records_for(team_id)
    EmployeesTeam.where(teams_id: team_id, active: true)
  end
end
