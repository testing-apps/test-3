# frozen_string_literal: true

class UserRole < ApplicationRecord
  enum role: %i[employee admin]

  after_initialize do
    self.role ||= :employee if new_record?
  end
end
