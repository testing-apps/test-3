# frozen_string_literal: true

class ApplicationController < ActionController::Base
  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found

  protect_from_forgery with: :exception

  add_flash_types :danger, :info, :warning, :success

  before_action :set_cache_buster

  def set_cache_buster
    response.headers['Cache-Control'] = 'no-cache, no-store, max-age=0, must-revalidate'
    response.headers['Pragma'] = 'no-cache'
    response.headers['Expires'] = 'Fri, 01 Jan 1990 00:00:00 GMT'
  end

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  helper_method :current_user

  def authorize
    redirect_to '/login' unless current_user
  end

  def employee?
    redirect_to home_path, danger: 'Cannot perform action as user: Admin' if current_user.as_employee.nil?
  end

  def record_not_found
    render :file => 'public/404.html', :status => :not_found
  end
end
