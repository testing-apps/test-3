# frozen_string_literal: true

initial_admin = User.create(user_name: 'admin',
                            password_digest: BCrypt::Password.create('password'))

UserRole.create(users_id: initial_admin.id, role: 'admin')
