# frozen_string_literal: true

class CreateTeams < ActiveRecord::Migration[5.2]
  def change
    create_table :teams do |t|
      t.string :name
      t.bigint :lead_id
      t.boolean :active

      t.timestamps
    end
  end
end
