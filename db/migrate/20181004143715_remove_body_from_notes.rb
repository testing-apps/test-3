# frozen_string_literal: true

class RemoveBodyFromNotes < ActiveRecord::Migration[5.2]
  def change
    remove_column :notes, :body, :text
  end
end
