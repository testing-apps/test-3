# frozen_string_literal: true

class CreateEmployee < ActiveRecord::Migration[5.2]
  def change
    create_table :employees do |t|
      t.string :first_name, null: false, limit: 40
      t.string :last_name, null: false, limit: 40
      t.string :email_address, null: true, limit: 80
      t.references :users, foreign_key: true

      t.timestamps
    end
  end
end
