# Test App 3

This repo has the following bugs:
- Log out > In address bar, add '/notes' or '/notes/1' to end of URL > Page should only be visible if logged in
- Settings > Change Password > Enter correct information and Save > Log in with new password > It will fail because every time you try to change a password it sets it back to the default "password"
- Log in as employee > Create a New Note > Choose a team for this note > all employees populate and do not hide when box is unchecked
- Log in as employee > Create a note > Include Roadblocks > Save new note > View new note > Under 'Actions', click Solutions > add Solution > Save > 404 red screen
- Go to any employee's history > “Pod History” is always empty


# Seeding

```ruby
rake db:create 
rake db:seed 
rake teams:seed_team
```
